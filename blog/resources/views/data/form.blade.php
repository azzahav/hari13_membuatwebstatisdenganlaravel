<!DOCTYPE html>
<html>
    <head>
        <title>SignUP</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
            <form action="\beranda">
                <div>
                    <p>First name:</p>
                    <input type="text" id="fname" name="fname" placeholder="Masukkan Firstname lurr"><br><br>
                    <label for="lname">Last name:</label><br><br>
                    <input type="text" id="lname" name="lname" placeholder="Masukkan Lastname lurr">
                </div>
                <div>
                    <p>Gender:</p>
                    <input type="radio" id="male" name="gender" value="male">
                    <label for="male">Male</label><br>
                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label><br>
                    <input type="radio" id="other" name="gender" value="other">
                    <label for="other">Other</label>
                </div>
                <div>
                    <p>Nationality:</p>
                    <select name="kebangsaan" id="kebangsaan">
                        <option value="indo">Indonesian</option>
                        <option value="sing">Singapuran</option>
                        <option value="malay">Malaysian</option>
                        <option value="aus">Australian</option>
                    </select>
                </div>
                <div>
                    <p>Language Spoken:</p>
                    <input type="checkbox" id="indonesia" name="bahasa" value="indonesia">
                    <label for="male">Bahasa Indonesia</label><br>
                    <input type="checkbox" id="inggris" name="bahasa" value="inggris">
                    <label for="female">English</label><br>
                    <input type="checkbox" id="other" name="bahasa" value="other">
                    <label for="other">Other</label>
                </div>
                <div>
                    <p>BIO:</p>                   
                    <textarea cols="30" rows="7" id="bio" placeholder="Masukkan Biomu lurr"></textarea>
                </div>
                <button type="submit" >Sign Up</button>
              </form>
    </body>
</html>